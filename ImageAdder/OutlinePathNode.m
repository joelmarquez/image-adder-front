//
//  OutlineNode.m
//  ImageAdder
//
//  Created by Daniel D'Abate on 10/17/14.
//  Copyright (c) 2014 Joel Marquez. All rights reserved.
//

#import "OutlinePathNode.h"

@implementation OutlinePathNode

+ (OutlinePathNode *)outlinePathNodeWithPathComponent:(NSString*)newPathComponent {
    OutlinePathNode * node = [[OutlinePathNode alloc] init];
    node.pathComponent = newPathComponent;
    
    return node;
}

-(id)init {
    self = [super init];
    
    if (self) {
        self.childs = [NSMutableArray array];
    }
    
    return self;
}

- (OutlinePathNode*)addPathComponent:(NSString*)pathComponent {
    for (OutlinePathNode * node in self.childs) {
        if ([node.pathComponent isEqualToString:pathComponent]) {
            return node;
        }
    }
    
    OutlinePathNode * newNode = [OutlinePathNode outlinePathNodeWithPathComponent:pathComponent];
    [self.childs addObject:newNode];
    
    return newNode;
}

- (NSUInteger)count {
    return [self.childs count];
}

- (OutlinePathNode*)childAtIndex:(NSUInteger)index {

    return (index < self.count) ? [self.childs objectAtIndex:index] : nil;
}

@end