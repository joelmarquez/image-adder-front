//
//  OutlineNode.h
//  ImageAdder
//
//  Created by Daniel D'Abate on 10/17/14.
//  Copyright (c) 2014 Joel Marquez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OutlinePathNode : NSObject

@property (strong, nonatomic) NSString * pathComponent;
@property (strong, nonatomic) NSString * path;

@property (strong, nonatomic) NSMutableArray * childs;

+ (OutlinePathNode *)outlinePathNodeWithPathComponent:(NSString*)newPathComponent;

- (OutlinePathNode*)addPathComponent:(NSString*)pathComponent;
- (NSUInteger)count;
- (OutlinePathNode*)childAtIndex:(NSUInteger)index;

@end
