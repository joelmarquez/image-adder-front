//
//  main.m
//  ImageAdder
//
//  Created by Joel Marquez on 10/15/14.
//  Copyright (c) 2014 Joel Marquez. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
