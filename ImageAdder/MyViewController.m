//
//  MyViewController.m
//  ImageAdder
//
//  Created by Joel Marquez on 10/15/14.
//  Copyright (c) 2014 Joel Marquez. All rights reserved.
//

#import "MyViewController.h"
#import "OutlinePathNode.h"

#define ORIGIN_DIR_KEY        @"ORIGIN_DIR_KEY"
#define DESTINATION_DIR_KEY   @"DESTINATION_DIR_KEY"
#define USE_CHKBOX_1X_KEY     @"USE_CHKBOX_1X_KEY"
#define USE_CHKBOX_2X_KEY     @"USE_CHKBOX_2X_KEY"
#define USE_CHKBOX_3X_KEY     @"USE_CHKBOX_3X_KEY"
#define USE_CHKBOX_IPHONE_KEY @"USE_CHKBOX_IPHONE_KEY"
#define USE_CHKBOX_IPAD_KEY   @"USE_CHKBOX_IPAD_KEY"

@interface MyViewController ()

@property (nonatomic, weak  ) IBOutlet NSTextField * sourceDirectoriesTxtField;
@property (nonatomic, weak  ) IBOutlet NSTextField * destinationDirectoriesTxtField;

@property (nonatomic, weak  ) IBOutlet NSButton    * sourceDirectoryBtn;
@property (nonatomic, weak  ) IBOutlet NSButton    * destinationDirectoryBtn;

@property (nonatomic, weak  ) IBOutlet NSButton    * btnCopyImages;

@property (nonatomic, strong) OutlinePathNode   * pathTree;

@property (nonatomic, weak  ) IBOutlet NSOutlineView    *logTree;
@property (nonatomic, weak  ) IBOutlet NSTextField      * logLbl;

@property (nonatomic, strong) IBOutlet NSProgressIndicator * progress;

@property (weak) IBOutlet NSButton *use1XCHKBox;
@property (weak) IBOutlet NSButton *use2XCHKBox;
@property (weak) IBOutlet NSButton *use3XCHKBox;
@property (weak) IBOutlet NSButton *useiPhoneCHKBox;
@property (weak) IBOutlet NSButton *useiPadCHKBox;

@property (weak) IBOutlet NSImageView *imagePreview;
@property (weak) IBOutlet NSTextField *lblNamePreview;
@property (weak) IBOutlet NSTextField *lblModifiedPreview;
@property (weak) IBOutlet NSTextField *lblSizePreview;
@property (weak) IBOutlet NSTextField *lblDimensionsPreview;

@end

@implementation MyViewController

- (void)restoreDefaults {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *sourceDir = [userDefaults stringForKey:ORIGIN_DIR_KEY];
    self.sourceDirectoriesTxtField.stringValue = sourceDir != nil ? sourceDir : @"";
    NSString *destDir = [userDefaults stringForKey:DESTINATION_DIR_KEY];
    self.destinationDirectoriesTxtField.stringValue = destDir != nil ? destDir : @"";
    NSInteger checkBoxValue = [userDefaults integerForKey:USE_CHKBOX_1X_KEY];
    self.use1XCHKBox.state = checkBoxValue;
    checkBoxValue = [userDefaults integerForKey:USE_CHKBOX_2X_KEY];
    self.use2XCHKBox.state = checkBoxValue;
    checkBoxValue = [userDefaults integerForKey:USE_CHKBOX_3X_KEY];
    self.use3XCHKBox.state = checkBoxValue;
    checkBoxValue = [userDefaults integerForKey:USE_CHKBOX_IPHONE_KEY];
    self.useiPhoneCHKBox.state = checkBoxValue;
    checkBoxValue = [userDefaults integerForKey:USE_CHKBOX_IPAD_KEY];
    self.useiPadCHKBox.state = checkBoxValue;
}

- (void)awakeFromNib {
    [super awakeFromNib];

    self.logTree.target = self;
    self.logTree.doubleAction = @selector(doubleClickOnItem:);
    self.logTree.action = @selector(singleClickOnItem:);
    
    [self.logLbl setHidden:YES];
    
    [self.sourceDirectoryBtn setState:NSOnState];
    [self.destinationDirectoryBtn setState:NSOnState];
    
    [self restoreDefaults];
    
    [self.sourceDirectoriesTxtField setEditable:NO];
    [self.destinationDirectoriesTxtField setEditable:NO];
}

- (void)doSelectDirectoryWithField:(NSTextField *)directoryField andButton:(NSButton *)directoryButton {
    
    NSOpenPanel *panel = [NSOpenPanel openPanel];
    panel.canChooseDirectories = YES;
    panel.canCreateDirectories = YES;
    panel.canChooseFiles = NO;
    panel.allowsMultipleSelection = NO;

    self.logLbl.stringValue = @"";
    
    if ([self isNotEmptyString:directoryField.stringValue]) {
        panel.directoryURL = [NSURL fileURLWithPath:directoryField.stringValue isDirectory:YES];
    }
    
    // displa]y the panel
    [panel beginWithCompletionHandler:^(NSInteger result) {
        if (result == NSFileHandlingPanelOKButton) {
            
            // grab a reference to what has been selected
            NSURL *directory = [[panel URLs] objectAtIndex:0];
            
            // write our file name to a label
            directoryField.stringValue = [directory path];;
        }
        
        [directoryButton setState:NSOnState];
    }];
}

- (IBAction)selectSourceDirectory:(NSButton *)sender {
    
    [self doSelectDirectoryWithField:self.sourceDirectoriesTxtField andButton:self.sourceDirectoryBtn];
}

- (IBAction)openSourceDirectory:(id)sender {
    [[NSWorkspace sharedWorkspace] selectFile:self.sourceDirectoriesTxtField.stringValue inFileViewerRootedAtPath:self.sourceDirectoriesTxtField.stringValue];
}

- (IBAction)selectDestinationDirectory:(NSButton *)sender {
    
    [self doSelectDirectoryWithField:self.destinationDirectoriesTxtField andButton:self.destinationDirectoryBtn];
}

- (IBAction)openDestinationDirectory:(id)sender {
    [[NSWorkspace sharedWorkspace] selectFile:self.destinationDirectoriesTxtField.stringValue inFileViewerRootedAtPath:self.destinationDirectoriesTxtField.stringValue];
}

- (void)showAlert {

    [self.logLbl setHidden:YES];
    
    NSString *informativeText = [NSString stringWithFormat:@"Las imágenes serán copiadas desde la carpeta \n\n%@ \n\nhacia la carpeta \n\n%@", self.sourceDirectoriesTxtField.stringValue, self.destinationDirectoriesTxtField.stringValue];
    
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:@"¿Estás seguro que deseas copiar las imágenes?"];
    [alert setInformativeText:informativeText];
    [alert addButtonWithTitle:@"OK"];
    [alert addButtonWithTitle:@"Cancelar"];
    [alert setAlertStyle:NSWarningAlertStyle];
    [alert beginSheetModalForWindow:self.view.window modalDelegate:self didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];
}

- (void)saveDefaults {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:self.sourceDirectoriesTxtField.stringValue forKey:ORIGIN_DIR_KEY];
    [userDefaults synchronize];
    [userDefaults setObject:self.destinationDirectoriesTxtField.stringValue forKey:DESTINATION_DIR_KEY];
    [userDefaults synchronize];
    [userDefaults setInteger:self.use1XCHKBox.state forKey:USE_CHKBOX_1X_KEY];
    [userDefaults synchronize];
    [userDefaults setInteger:self.use2XCHKBox.state forKey:USE_CHKBOX_2X_KEY];
    [userDefaults synchronize];
    [userDefaults setInteger:self.use3XCHKBox.state forKey:USE_CHKBOX_3X_KEY];
    [userDefaults synchronize];
    [userDefaults setInteger:self.useiPhoneCHKBox.state forKey:USE_CHKBOX_IPHONE_KEY];
    [userDefaults synchronize];
    [userDefaults setInteger:self.useiPadCHKBox.state forKey:USE_CHKBOX_IPAD_KEY];
    [userDefaults synchronize];
}

- (BOOL)checkBoxesValidation {
    
    BOOL result = true;
    
    if (!self.useiPhoneCHKBox.state && !self.useiPadCHKBox.state) {
        result = false;
    }
    else if (!self.use1XCHKBox.state && !self.use2XCHKBox.state && !self.use3XCHKBox.state) {
        result = false;
    }
    
    return result;
}

- (void)errorLog:(NSString *)errorMsg {
    self.logLbl.stringValue = errorMsg;
    self.logLbl.textColor = [NSColor redColor];
    self.logLbl.hidden = NO;
}

- (IBAction)copyImages:(NSButton *)sender {
    
    if ([self isNotEmptyString:self.sourceDirectoriesTxtField.stringValue] && [self isNotEmptyString:self.destinationDirectoriesTxtField.stringValue]) {
        if ([self checkBoxesValidation]) {
            self.logLbl.hidden = YES;
            [self saveDefaults];
            [self showAlert];
        }
        else {
            [self errorLog:@"ERROR: Debes seleccionar algún checkbox"];
        }
    }
    else {
        [self errorLog:@"ERROR: Alguno de los directorios no fue seleccionado"];
    }
}

- (NSArray *)getTaskArguments {
    
    NSArray *initial = @[@"-jar", @"image-adder.jar", @"-od", self.sourceDirectoriesTxtField.stringValue, @"-dd", self.destinationDirectoriesTxtField.stringValue];
    NSMutableArray *result = [NSMutableArray arrayWithArray:initial];
    
    if (self.use1XCHKBox.state) {
        [result addObject:@"-1"];
    }
    if (self.use2XCHKBox.state) {
        [result addObject:@"-2"];
    }
    if (self.use3XCHKBox.state) {
        [result addObject:@"-3"];
    }
    if (self.useiPhoneCHKBox.state) {
        [result addObject:@"-h"];
    }
    if (self.useiPadCHKBox.state) {
        [result addObject:@"-a"];
    }
    
    return result;
}

- (void)runCopyImagesScript {
    
    [self.btnCopyImages setHidden:YES];
    [self.progress performSelector:@selector(startAnimation:) withObject:self afterDelay:0.1f];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSPipe *pipe = [NSPipe pipe];
        NSFileHandle *file = pipe.fileHandleForReading;
        
        NSTask *task = [[NSTask alloc] init];
        task.launchPath = @"/usr/bin/java";
        NSString *pathForJar = [[[NSBundle mainBundle] pathForResource:@"image-adder" ofType:@".jar"] stringByDeletingLastPathComponent];
        task.currentDirectoryPath = pathForJar;
        task.arguments = [self getTaskArguments];
        task.standardOutput = pipe;
        
        NSMutableData *data = [NSMutableData dataWithCapacity:512];
        [task launch];
        while ([task isRunning]) {
            [data appendData:[file readDataToEndOfFile]];
        }
        [data appendData:[file readDataToEndOfFile]];
        [file closeFile];
        
        __block NSString *scriptOutput = [[NSString alloc] initWithData:data encoding:NSMacOSRomanStringEncoding];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.progress stopAnimation:self];
            [self.btnCopyImages setHidden:NO];
            [self.logLbl setHidden:NO];
        
            NSArray *lines = [scriptOutput componentsSeparatedByString:@"\n"];
            
            self.pathTree = [self treeFromFileList:lines];
            [self cleanPathTree];
            
            [self.logTree reloadData];
            [self.logTree expandItem:self.pathTree];
            
            NSString *s = lines.count -1 == 1 ? @"" : @"s";
            self.logLbl.stringValue = lines.count == 0 ? @"No hay archivos nuevos" : [NSString stringWithFormat:@"Hay %lu archivo%@ nuevo%@ para agregar", (unsigned long)lines.count - 1, s, s];
            self.logLbl.textColor = [NSColor blackColor];
        });
    });
}

- (void) alertDidEnd:(NSAlert *)alert returnCode:(int)returnCode contextInfo:(int *)contextInfo {
    
    switch (returnCode) {
        case NSAlertFirstButtonReturn:
            [self runCopyImagesScript];
            break;
            
        default:
            break;
    }
}

- (BOOL)isNotEmptyString:(NSString *)string {
    return string != nil && ![string isEqualToString:@""];
}

- (OutlinePathNode *) treeFromFileList:(NSArray*)files {
    
    OutlinePathNode * tree = [OutlinePathNode outlinePathNodeWithPathComponent:@""];
    
    for (NSString * file in files) {
        OutlinePathNode * currentTree = tree;
        
        for (NSString * pathComponent in file.pathComponents) {
            currentTree = [currentTree addPathComponent:pathComponent];
        }
        
        currentTree.path = file;
    }
    
    return tree;
}

- (void) cleanPathTree {
    if (self.pathTree.count == 1) {
        OutlinePathNode * child = [self.pathTree childAtIndex:0];
        
        child.pathComponent = [self.pathTree.pathComponent stringByAppendingPathComponent: child.pathComponent];
        
        self.pathTree = child;
        
        [self cleanPathTree];
    }
}

-(void)changePreviewForItem:(OutlinePathNode*)item {
    NSImage * image = [[NSImage alloc] initWithContentsOfFile:item.path];
    self.imagePreview.image = image;
    
    NSDictionary * attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:item.path error:nil];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy hh:mma"];
    NSString *dateString = [dateFormat stringFromDate:[attributes fileModificationDate]];
    
    self.lblNamePreview.stringValue = item.pathComponent;
    self.lblModifiedPreview.stringValue = dateString;
    self.lblSizePreview.stringValue = [NSByteCountFormatter stringFromByteCount:[attributes fileSize] countStyle:NSByteCountFormatterCountStyleFile];
    
    CGFloat imageScale = [image recommendedLayerContentsScale:1.0f];
    self.lblDimensionsPreview.stringValue = [NSString stringWithFormat:@"%.0f x %.0f", image.size.width * imageScale, image.size.height * imageScale];
}

#pragma mark - NSOutlineViewDataSource & NSOutlineViewDelegate

// Method returns count of children for given tree node item
- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item {

    return !item ? self.pathTree != nil : [(OutlinePathNode*)item count];
}

// Method returns flag, whether we can expand given tree node item or not
// (here is the simple rule, we can expand only nodes having one and more children)
- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item {

    return [(OutlinePathNode*)item count] > 0;
}

// Method returns value to be shown for given column of the tree node item
- (id) outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item {

    return [(OutlinePathNode*)item pathComponent];
}

// Method returns children item for given tree node item by given index
- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item {

    return !item ? self.pathTree : [(OutlinePathNode*)item childAtIndex:index];
}

- (BOOL)selectionShouldChangeInOutlineView:(NSOutlineView *)outlineView {
    
    OutlinePathNode * item = [outlineView itemAtRow:[outlineView clickedRow]];
    return item.count == 0;
}

- (void)doubleClickOnItem:(id)sender {
    OutlinePathNode * item = [sender itemAtRow:[sender clickedRow]];
    
    if (item.count > 0) {
        if ([sender isItemExpanded:item]) {
            [sender collapseItem:item];
        }
        else {
            [sender expandItem:item];
        }
    }
    else {
        [[NSWorkspace sharedWorkspace] selectFile:item.path inFileViewerRootedAtPath:[item.path stringByDeletingLastPathComponent]];
        
        [self changePreviewForItem:item];
    }
}

- (void)singleClickOnItem:(id)sender {
    OutlinePathNode * item = [sender itemAtRow:[sender clickedRow]];

    if (item.count == 0) {
        [self changePreviewForItem:item];
    }
}

@end