//
//  AppDelegate.h
//  ImageAdder
//
//  Created by Joel Marquez on 10/15/14.
//  Copyright (c) 2014 Joel Marquez. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

